# Simple Working Hours Counter

## Installation
* `git clone https://gitlab.com/rdi-eg/misc/working_hours.git`
* `cd working_hours`
* `chmod +x working_hours.py`
* `sudo cp working_hours.py /usr/local/bin`

## Usage
```
~ ❯ working_hours.py 
Usage:
        working_hours.py status
                Prints your working hours status
        working_hours.py start
                Starts counting hours
        working_hours.py stop
                Stops counting hours
        working_hours.py clear
                Clears any saved hours
```