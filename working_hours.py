#!/usr/bin/env python3

import os
import sys
import time
import pathlib

config_directory = str(pathlib.Path.home()) + '/.working_hours/'
working_hours_file = config_directory + 'working_hours'
working_toggle_file = config_directory + 'working'


def usage():
	print("Usage:")
	print("\tworking_hours.py status")
	print("\t\tPrints your working hours status")
	print("\tworking_hours.py start")
	print("\t\tStarts counting hours")
	print("\tworking_hours.py stop")
	print("\t\tStops counting hours")
	print("\tworking_hours.py clear")
	print("\t\tClears any saved hours")

def is_working():
	if not os.path.isfile(working_hours_file):
		return False
	
	with open(working_hours_file, 'r') as file:
		file_content = file.read()
		if file_content[-1] == "\n":
			return False
		elif file_content[-1] == ":":
			return True
		
		print(working_hours_file + " is in an invalid state. Exiting...")
		exit()

def calculate_working_hours():
	if not os.path.isfile(working_hours_file):
		return 0
	
	file = open(working_hours_file)
	lines = file.readlines()
	file.close()

	total_seconds = 0

	for l in lines:
		tokens = l.split(':')

		if(len(tokens[1]) == 0):
			total_seconds += int(time.time()) - int(tokens[0])
			break

		total_seconds += int(tokens[1]) - int(tokens[0])
	
	return total_seconds / 3600 # / 3600 to convert to hours

def status():
	if is_working():
		print("Your current status is: Working")
	else:
		print("Your current status is: Not working")
	
	print("Total working hours: " + str(calculate_working_hours()))

def start():
	if is_working():
		print("I'm already started.")
		return
	
	if os.path.isfile(working_hours_file):
		file = open(working_hours_file, 'a')
	else:
		pathlib.Path(config_directory).mkdir(parents=True, exist_ok=True)
		file = open(working_hours_file, 'w')

	file.write(str(int(time.time())) + ":")
	file.close()

def stop():
	if not is_working():
		print("I'm already stopped.")
		return
	
	file = open(working_hours_file, 'a')
	file.write(str(int(time.time())) + "\n")
	file.close()

def clear():
	if os.path.exists(working_hours_file):
		os.remove(working_hours_file)
	print("Cleared.")

if len(sys.argv) == 1 or len(sys.argv) > 2:
	usage()
	exit()

if sys.argv[1] == 'status':
	status()
	exit()

if sys.argv[1] == 'start':
	start()
	status()
	exit()

if sys.argv[1] == 'stop':
	stop()
	status()
	exit()

if sys.argv[1] == 'clear':
	clear()
	exit()

print("Unknown command: " + sys.argv[1])
usage()
exit()